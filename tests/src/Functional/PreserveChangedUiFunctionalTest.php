<?php

namespace Drupal\Tests\preserve_changed_ui\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group preserve_changed_ui
 */
class PreserveChangedUiFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'test_page_test',
    'preserve_changed_ui',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);

    $this->createContentType(['type' => 'article']);
    \Drupal::entityTypeManager()
      ->getStorage('entity_form_display')
      ->load('node.article.default')
      ->setComponent('preserve_changed_time')->save();
  }

  /**
   * Tests if installing the module, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    // Ensure the status code is success:
    $session->statusCodeEquals(200);
    // Ensure the correct test page is loaded as front page:
    $session->pageTextContains('Test page text.');
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    // Go to uninstallation page an uninstall preserve_changed_ui:
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-preserve-changed-ui');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm uninstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
    // Retest the frontpage:
    $this->drupalGet('<front>');
    // Ensure the status code is success:
    $session->statusCodeEquals(200);
    // Ensure the correct test page is loaded as front page:
    $session->pageTextContains('Test page text.');
  }

  /**
   * Tests if the field actually exists inside a node.
   */
  public function testFieldExists() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to article creation page and check if the field isn't shown:
    $this->drupalGet('/node/add/article');
    $session->elementNotExists('css', '#edit-preserve-changed-time-value');
    // Create a node and see if the field exists on edit:
    $this->createNode(['type' => 'article', 'id' => 1, 'title' => 'test123']);
    $this->drupalGet('/node/1/edit');
    $session->elementExists('css', '#edit-preserve-changed-time-value');
    // Check default installation value:
    $session->checkboxNotChecked('edit-preserve-changed-time-value');
    // Check the field, save and see if it is changed to 0 again after
    // revisiting:
    $page->checkField('edit-preserve-changed-time-value');
    $page->pressButton('edit-submit');
    $this->drupalGet('/node/1/edit');
    $session->checkboxNotChecked('edit-preserve-changed-time-value');
    $this->drupalGet('/admin/structure/types/manage/article/form-display');
  }

}
