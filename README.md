# Preserve Changed Timestamp UI
Provides a checkbox in node edit forms to preserve the last changed date for the
current change. Typically used for minor changes like typo fixes, if you don't
want such a change to be reflected by a changed updated date.

If you need an API module instead of the UI, see
https://www.drupal.org/project/preserve_changed

## Setup
- Require the module via composer using `composer require preserve_changed_ui`
- Install the module using drush `drush en preserve_changed_ui` or via the UI

## Usage
Go to `/admin/config/system/preserve-changed-ui` and define the default
behaviour of the module. After that, make sure the field is enabled on the
node bundle you want to use it on.

Now when you edit a node, there is a checkbox on the bottom of the form, to set
whether you want to preserve the "Last saved" timestamp on the current
node.
