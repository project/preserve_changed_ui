<?php

namespace Drupal\preserve_changed_ui\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Preserve Changed Ui settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'preserve_changed_ui_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['preserve_changed_ui.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['enable_preserve_changed_time'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Preserve Changed Time'),
      '#description' => $this->t('Set the default behaviour for preserving the "Last saved" timestamp on ALL node types (you are still able to individually check/uncheck this option on the node edit page).'),
      '#default_value' => $this->config('preserve_changed_ui.settings')->get('enable_preserve_changed_time'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('preserve_changed_ui.settings')
      ->set('enable_preserve_changed_time', $form_state->getValue('enable_preserve_changed_time'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
